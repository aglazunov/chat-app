from flask import Flask
from peewee import SqliteDatabase

DATABASE = 'chat.db'
database = SqliteDatabase(DATABASE)

app = Flask(__name__)