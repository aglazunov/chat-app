import datetime
from app import app
from models import *
from flask import jsonify, g, request

@app.before_request
def before_request():
    g.db = database
    g.db.connect()

@app.after_request
def after_request(response):
    g.db.close()
    return response

@app.route('/api/messages/', methods=['GET', 'POST'])
def messages_endpoint():
    if request.method == 'POST':
        return add_message()
    elif request.method == 'GET':
        return get_messages()

def get_messages():
    result = Message.select().order_by(Message.time).dicts()
    return jsonify([entry for entry in result])

def add_message():
    Message.create(
        user=request.form['user'],
        content=request.form['content'],
        time=datetime.datetime.now()
    )
    return jsonify({
        "status": "success"
    })

@app.route('/')
def hello_world():
    return 'Hello World!'