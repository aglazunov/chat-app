from peewee import *
from app import database

class BaseModel(Model):
    class Meta:
        database = database


class Message(BaseModel):
    user = TextField()
    content = TextField()
    time = DateTimeField()