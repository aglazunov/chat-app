from app import app, database
from models import *
import routes
import logging

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

CLEAN_START = True
def create_tables():
    models = [Message]
    with database:
        CLEAN_START and database.drop_tables(models)
        database.create_tables(models)

if __name__ == '__main__':
    create_tables()
    app.run(port=7702, debug=False)