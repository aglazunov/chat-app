import React from 'react';
import Message from './Message';

import './MessageList.css';

class MessageList extends React.Component {
    constructor(props) {
        super(props);

        this.listRef = React.createRef();
    }

    scrollToBottom = () => {
        const element = this.listRef.current;
        element && (element.scrollTop = element.scrollHeight - element.clientHeight);
    };

    handleScrollChange = () => {
        const element = this.listRef.current;
        const threshold = 10;

        if (element) {
            const {autoscroll} = this.props;
            const shouldAutoScroll = element.scrollTop + element.clientHeight >= element.scrollHeight - threshold;

            if (autoscroll !== shouldAutoScroll) {
                this.props.onChangeAutoscroll(shouldAutoScroll);
            }
        }
    };

    render() {
        const {messages, autoscroll} = this.props;

        autoscroll && this.scrollToBottom();

        return <div className="message-list" ref={this.listRef} onScroll={this.handleScrollChange}>
            {messages.map(message => <Message key={message.id} {...message}/>)}
        </div>;
    }
}

export default MessageList;