import React from 'react';
import moment from 'moment';
import {getUserColor} from "../util/helpers";

import './Message.css';

const Message = ({user, content, time}) => {
    return <div className="chat-message" style={{background: getUserColor(user)}}>
        <div className="user">{user}</div>
        <div className="content">
            {content.split('\n').map((line, index) =>
                <div className="message-line" key={index}>{line}</div>)}
        </div>
        <div className="time">{moment(time).calendar()}</div>
    </div>;
};

export default Message;