import React from 'react';
import './SendMessage.css';
import {getUserColor} from "../util/helpers";
import sendIcon from '../assets/send.svg';

class SendMessage extends React.Component {
    state = {
        message: '',
        textRows: 1,
        sending: false,
        error: null
    };

    constructor(props) {
        super(props);

        this.field = React.createRef();
    }

    componentDidMount() {
        this.field.current && this.field.current.focus();
    }

    handleChangeMessage = (event) => {
        let message = event.target.value;
        this.setState({
            message: message,
            textRows: message.split('\n').length
        });
    };

    clearMessage = () => {
        this.setState({
            message: '',
            textRows: 1
        });
    };

    handleSend = async () => {
        const {message} = this.state;

        if (message) {
            this.setState({
                sending: true,
                error: null
            });
            try {
                await this.props.onSend(message);
                this.clearMessage();
            } catch (e) {
                this.setState({
                    error: e.message
                });
            }

            this.setState({
                sending: false
            });
        }
    };

    handleKeyDown = (event) => {
        if (event.key === 'Enter' && !event.metaKey && !event.shiftKey && !event.ctrlKey) {
            this.handleSend();
            event.preventDefault();
        }
    };

    render() {
        const {message, textRows, sending, error} = this.state;
        const {user} = this.props;
        const userColor = getUserColor(user);
        const placeholder = sending ? 'Sending…' : "Write a message…";
        const errorMessage = error ? `Error sending message: ${error}` : null;

        return <div className={["send-message-area", error ? 'error' : ''].join(' ').trim()} style={{borderColor: userColor}}>
            <textarea rows={textRows}
                      value={sending ? '' : message}
                      ref={this.field}
                      title={errorMessage}
                      onChange={this.handleChangeMessage}
                      onKeyDown={this.handleKeyDown}
                      placeholder={placeholder}
                      className="send-message-field"
            />
            <button onClick={this.handleSend} className="send-message-button"
                    style={{background: userColor}}>
                <img src={sendIcon} alt="Send"/>
            </button>
        </div>;
    }
}

export default SendMessage;