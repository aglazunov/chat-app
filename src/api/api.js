const ENDPOINT = 'api';

const handleErrors = (response) => {
    if (!response.ok) {
        throw new Error(response.statusText);
    }
    return response;
};

const handleJson = response => {
    if (!(response instanceof Object) || typeof response.json !== 'function') {
        throw new Error('Invalid response format (must be json)');
    }
    return response.json();
};

export const getMessages = () => fetch(`${ENDPOINT}/messages/`)
    .then(handleErrors)
    .then(handleJson);

export const postMessage = (user, content) => {
    const formData = new FormData();
    formData.append('user', user);
    formData.append('content', content);

    const fetchParams = {
        method: 'POST',
        body: formData
    };

    return fetch(`${ENDPOINT}/messages/`, fetchParams)
        .then(handleErrors);
};
