import React, { Component } from 'react';
import './App.css';
import * as api from '../api/api';
import {getRandomUserName} from '../util/helpers';

import MessageList from '../components/MessageList';
import SendMessage from '../components/SendMessage';

class App extends Component {
  state = {
      messages: [],
      error: null,
      autoscroll: true
  };

  componentDidMount() {
      this.refreshMessages();
      this.setState({
          userName: getRandomUserName()
      });

      setInterval(this.refreshMessages.bind(this), 1000);
  }

  handleChangeAutoscroll = (autoscroll) => {
      this.setState({
          autoscroll
      });
  };

  async refreshMessages() {
      try {
          const messages = await api.getMessages();
          this.setState({
              error: null,
              messages
          })
      } catch (e) {
          this.setState({
              error: e.message,
              messages: []
          })
      }
  }

  sendMessage = (message) => api.postMessage(this.state.userName, message)
      .then(this.refreshMessages())
      .then(this.handleChangeAutoscroll(true));

  render() {
    const {messages, userName, error} = this.state;

    return (
      <div className="App">
          {error ?
              <div className="error-display">Error getting messages: {error}</div>
              : <MessageList messages={messages} autoscroll={this.state.autoscroll} onChangeAutoscroll={this.handleChangeAutoscroll}/>}
        <SendMessage onSend={this.sendMessage} user={userName || ''}/>
      </div>
    );
  }
}

export default App;
