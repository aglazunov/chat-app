/*
 * Go to http://random-name-generator.info/random/?n=50&g=1&st=2
 * Put this in console:
 * [...document.getElementsByClassName('nameList')[0].getElementsByTagName('li')].map(e => e.textContent.split(' ')[0].trim()).filter((element, index, array) => array.indexOf(element) === index)
 */

const names = ["Delia", "Garrett", "Juanita", "Marie", "Guy", "Jonathan", "Carrie", "Traci", "Kelley", "Gregory", "Fred", "Jeffrey", "Marty", "Angelo", "Owen", "Melody", "Ray", "Bennie", "Laverne", "Kim", "Lee", "Gordon", "Kent", "Lynda", "Glenn", "Rick", "Katie", "Lori", "Salvatore", "Jordan", "Leon", "Charlene", "Miguel", "Seth", "Grace", "Mae", "Violet", "Fernando", "Jean", "Mark", "Willard", "Dustin", "Kristin", "Chad", "Rita", "Gladys", "Lila", "Michelle", "Kayla", "Mitchell", "Rogelio", "Matt", "Tracy", "Muriel", "Shirley", "Vicki", "Sophia", "Dianne", "Timmy", "Ora", "Gregg", "Luz", "Elbert", "Alonzo", "Rolando", "Crystal", "Stacy", "Ronald", "Anthony", "Abraham", "Ana", "Jacquelyn", "Leona", "Jennie", "Daniel", "Jeremiah", "Eugene", "Ginger", "Andrew", "Erma", "Faye", "Lawrence", "Pam", "Marcella", "Virgil", "Jaime", "Vanessa", "Vernon", "Evan", "Carroll", "Peggy", "Eddie", "Eula", "Dawn", "Steven", "Teri", "Linda", "Franklin", "Spencer"];

export const getRandomUserName = () => names[Math.round(Math.random() * (names.length - 1))];

export const getUserColor = (name) => {
    const getRatio = (name) => ([...name].map(s => s.charCodeAt(0)).reduce((x,y) => x+y, 0) % 100) / 100;
    return `hsla(${255 * getRatio(name)}, 81%, 94%, 1)`;
};